require "rails_helper"

RSpec.describe 'Album' do
  context '#most_repeated_words_in_title' do
    let!(:album)  { FactoryBot.create(:album, title: 'some string') }
    let!(:album2)  { FactoryBot.create(:album, title: 'another string') }

    it 'returns expected results' do
      results = Album.most_repeated_words_in_title

      expect(results.values[0][0]).to eq 'string'
      expect(results.values[0][1]).to eq 2
    end
  end
end
