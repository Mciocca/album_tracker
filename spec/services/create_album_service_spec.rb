require "rails_helper"

RSpec.describe 'CreateAlbumService' do
  let(:title) { 'some title' }
  let(:condition) { 'good' }
  let(:year) { 2000 }
  let(:artist_name) { 'Wierd Al' }
  let(:params) do
    {
      title: title,
      condition: condition,
      year: year,
      artist_name: artist_name,
    }
  end

  context 'without an exsiting artist' do
    before(:each) do
      FactoryBot.create(:artist, name: artist_name )
    end

    it "associates the new album with the correct artist" do
      service = CreateAlbumService.new(params)

      expect { service.save }.to_not change{ Artist.count }

      album = service.artist.albums.find_by(title: title)
      expect(album.title).to eq title
    end
  end

  context 'with a new artist' do
    it "creates a record for the artist" do
      service = CreateAlbumService.new(params)

      expect { service.save }.to change{ Artist.count }.by 1
    end
  end

  context 'validation errors' do
    let(:artist) { FactoryBot.create(:artist, name: artist_name) }
    let!(:duplicate) { FactoryBot.create(:album, title: title, artist: artist) }

    it "shows the full messages" do
      service = CreateAlbumService.new(params)
      expect(service.save).to be false
      expect(service.errors).to include "Title has already been taken"
    end
  end
end
