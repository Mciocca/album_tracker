FactoryBot.define do
  factory :album do
    sequence(:title) { |n| "Title #{n}" }
    condition { ['good', 'poor', 'excellent'].sample }
    year { rand(2000..2021) }
    artist
  end
end
