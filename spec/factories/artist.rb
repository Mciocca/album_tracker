FactoryBot.define do
  factory :artist do
    sequence(:name) { |n| "Name #{n}" }
  end
end
