require "rails_helper"

RSpec.describe "AlbumQuery" do
  let!(:all_albums) { FactoryBot.create_list(:album, 5) }
  let(:artist) { FactoryBot.create(:artist) }

  context "when given no arguments" do
    it "returns all albums" do
      albums = AlbumQuery.new.albums

      expect(albums.length).to eq Album.count
    end
  end

  context "single arguments" do
    let!(:target_album) {
      FactoryBot.create(
        :album,
        title: 'Running With Scissors',
        condition: 'custom condition',
        year: 12,
        artist: artist
      )
    }

    it 'title' do
      albums = AlbumQuery.new(title: target_album.title).albums

      expect(albums.count).to eq 1
      expect(albums[0].title).to eq target_album.title
    end

    it 'condition' do
      albums = AlbumQuery.new(condition: target_album.condition).albums

      expect(albums.count).to eq 1
      expect(albums[0].title).to eq target_album.title
    end

    it 'year' do
      albums = AlbumQuery.new(year: target_album.year).albums

      expect(albums.count).to eq 1
      expect(albums[0].title).to eq target_album.title
    end

    it 'artist_name' do
      albums = AlbumQuery.new(artist_name: artist.name).albums

      expect(albums.count).to eq 1
      expect(albums[0].title).to eq target_album.title
    end
  end

  context "multiple arguments" do
    let!(:album1) { FactoryBot.create(:album, year: 12, artist: artist)}
    let!(:album2) { FactoryBot.create(:album, year: 12, artist: artist)}
    let!(:album3) { FactoryBot.create(:album, year: 1,  artist: artist)}

    it "returns the expected records" do
      albums = AlbumQuery.new(artist_name: artist.name, year: 12).albums

      expect(albums.length).to eq 2
    end
  end
end
