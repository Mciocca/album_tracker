json.results @results do |result|
  json.word result["word"]
  json.occurrences result["ndoc"]
end
