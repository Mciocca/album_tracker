json.albums @albums do |album|
  json.(album, :id, :title, :condition, :year)
  json.artist(album.artist, :id, :name)
end

json.meta do
  json.total_pages @albums.total_pages
  json.next_page   @albums.next_page
  json.prev_page   @albums.prev_page
end
