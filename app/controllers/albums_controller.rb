class AlbumsController < ApplicationController
  def index
    @albums = AlbumQuery.new(
      title: params[:title],
      year:  params[:year],
      condition: params[:condition],
      artist_name: params[:artist_name]
    ).albums.page(params[:page] || 1)
  end

  def show
    album
  end

  def update
    if album.update(update_album_params)
      render json: album
    else
      render json: { error: album.errors.full_messages }
    end
  end

  def destroy
    if album.destroy
      head :ok
    else
      render json: { error: album.errors.full_messages }
    end
  end

  def create
    binding.pry
    service = CreateAlbumService.new(create_album_params)

    if service.save
      render json: service.album
    else
      render json: { errors: service.errors  }
    end
  end

  def most_repeated
    @results = Album.most_repeated_words_in_title
  end

  private

  def album
    @album ||= Album.find(params[:id])
  end

  def update_album_params
    params.require(:album).permit(:title, :year, :condition, artist_attributes: [:name])
  end

  def create_album_params
    params.require(:album).permit(:title, :year, :condition, :artist_name)
  end
end
