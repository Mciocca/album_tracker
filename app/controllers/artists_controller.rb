class ArtistsController < ApplicationController
  def index
    render json: Artist.all
  end

  def show
    render json: artist
  end

  def update
    if artist.update(artist_params)
      render json: artist
    else
      render json: { errors: artist.errors.full_messages }
    end
  end

  def create
    if Artist.create(artist_params)
      render json: @artist
    else
      render json: { errors: artist.errors.full_messages }
    end
  end

  def destroy
    if artist.destroy
      head :ok
    else
      render json: { errors: artist.errors.full_messages }
    end
  end

  def count_by_year
    render json: Artist.find(params[:id]).albums.group(:year).count
  end

  private

  def artist
    @artist ||= Artist.find(params[:id])
  end

  def artist_params
    params.require(:artist).permit(name)
  end
end
