class AlbumQuery
  attr_reader :title, :year, :condition, :artist_name

  def initialize(title: nil, year: nil, condition: nil, artist_name: nil)
    @title = title
    @year = year
    @condition = condition
    @artist_name = artist_name
  end

  def albums
    Album.eager_load(:artist)
         .merge(title_query)
         .merge(year_query)
         .merge(condition_query)
         .merge(artist_name_query)
  end

  private

  def title_query
    if title
      Album.where("title ILIKE ?", "%#{title}%")
    else
      Album.all
    end
  end

  def year_query
    if year
      Album.where(year: year)
    else
      Album.all
    end
  end

  def condition_query
    if condition
      Album.where(condition: condition)
    else
      Album.all
    end
  end

  def artist_name_query
    if artist_name
      Album.joins(:artist).where(artist: { name: artist_name })
    else
      Album.all
    end
  end
end

