class Album < ApplicationRecord
  belongs_to :artist

  validates :title, presence: true, uniqueness: { scope: :artist }
  validates :year, presence: true
  validates :condition, presence: true

  accepts_nested_attributes_for :artist

  def self.most_repeated_words_in_title
    sql =  <<-SQL
      SELECT *
      FROM ts_stat($$SELECT to_tsvector('english', title) from albums$$)
      ORDER BY ndoc DESC limit 5
    SQL

    ActiveRecord::Base.connection.execute(sql)
  end
end
