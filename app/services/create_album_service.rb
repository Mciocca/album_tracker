class CreateAlbumService
  attr_reader :title, :condition, :year, :artist_name, :album, :artist

  def initialize(params)
    @title = params[:title]
    @condition = params[:condition]
    @year = params[:year]
    @artist_name = params[:artist_name]
  end

  def save
    ActiveRecord::Base.transaction do
      @album = artist.albums.build(title: title, condition: condition, year: year)
      album.save!

      true
    end
  rescue StandardError
    false
  end

  def errors
    album.errors.full_messages + artist.errors.full_messages
  end

  def artist
    @artist ||= Artist.find_or_create_by(name: artist_name)
  end
end
