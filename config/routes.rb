Rails.application.routes.draw do
  get "albums/most_repeated" => "albums#most_repeated",  defaults: { format: :json }

  resources :artists, defaults: { format: :json }
  resources :albums, defaults: { format: :json }

  get "artists/:id/count_by_year" => "artists#count_by_year"
end
