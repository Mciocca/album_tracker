# Requirements
- Ruby 2.65
- Postgresql 9.65 or later


# Setup
- run `bundle install`
- run `rails db:setup`
- run `rails s`

Seeds should run during setup but can be optionally run by `rails db:seed`


# Running tests
- Run `rspec` for all tests
- Run `rspec /path/to/file` to run specific tests

# Possible Improvements
- More tests - this is lightly tested for time reasons. It would benefit a lot from request specs
- `ILike` for everything in `AlbumQuery`
- Nested urls like `/artist/2/albums`
- Move albums by year into normal artists response instead of it's own url

