class CreateAlbums < ActiveRecord::Migration[6.1]
  def change
    create_table :albums do |t|
      t.string  :title, null: false
      t.integer :year, null: false # feature requirements allow a simple data type
      t.string :condition, null: false
      t.references :artist, foreign_key: true

      t.timestamps
    end

    add_index(:albums, [:title, :artist_id], :unique => true)
  end
end
