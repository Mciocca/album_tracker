# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
require 'faker'

(1..20).each do |id|
  Artist.create!(
    name: Faker::Music.band + " #{id}"
  )
end

artist_ids = Artist.pluck(:id)

(1..100).each do |id|
  Album.create!(
    title: Faker::Music.album + " #{id}",
    condition: ['good', 'poor', 'excellent'].sample,
    year: rand(2000..2021),
    artist_id: artist_ids.sample
  )
end
